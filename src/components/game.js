import React, { Component } from 'react';

import { initialState, context } from 'src/engine/context';
import Footer from './footer';
import Play from './play';

import '../custom.css';

function partialUpdate(value, newValue) {
    const valueIsObject = typeof value === 'object' && value !== null && !Array.isArray(value);
    const newValueIsObject = typeof newValue === 'object' && newValue !== null && !Array.isArray(newValue);
    if (newValueIsObject && valueIsObject) {
        const newObj = { ...value };
        for (const key in newValue) {
            newObj[key] = partialUpdate(value[key], newValue[key]);
        }
        return newObj;
    } else {
        return newValue;
    }
}

export default class Game extends Component {
    constructor(props) {
        super(props);

        this.updateS = changes => this.setState(state => ({ s: partialUpdate(state.s, changes) }));
        this.saveS = () => window.localStorage.setItem('save0', JSON.stringify(this.state.s));

        this.state = {
            s: initialState,
            set: this.updateS,
            save: this.saveS,
        };
    }

    render() {
        return (
            <div className="game">
                <context.Provider value={this.state}>
                    <div className="main">
                        <Play />
                    </div>
                    <Footer />
                </context.Provider>
            </div>
        );
    }
}

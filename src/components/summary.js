import React, { Component } from 'react';
import { context } from 'src/engine/context';

export default class Summary extends Component {
    static contextType = context;

    render() {
        const you = this.context.s.you;
        return (
            <div className='playColumn summary'>
                <dl>
                    {you.name && <>
                        <dt>name</dt>
                        <dd>{you.name}</dd>
                    </>}
                    {you.status && <>
                        <dt>status</dt>
                        <dd>{you.status}</dd>
                    </>}
                    {you.mech?.name && <>
                        <dt>mech</dt>
                        <dd>{you.mech?.name}</dd>
                    </>}
                    <dt>memories</dt>
                    <dd>limited</dd>
                </dl>
            </div>
        );
    }
}

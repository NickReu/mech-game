import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './typer.css';

export default class Typer extends Component {
    static propTypes = {
        delay: PropTypes.number,
        text: PropTypes.string.isRequired,
        children: PropTypes.node,
    };

    constructor(props) {
        super(props);
        this.state = {
            revealed: 1,
            skipped: 0,
        };

        if (this.props.delay == undefined) {
            this.delay = 20;
        } else {
            this.delay = this.props.delay;
        }

        if (this.props.children == undefined) {
            this.children = [];
        } else {
            this.children = React.Children.toArray(this.props.children);
        }

        this.childrenCount = React.Children.count(this.props.children);
        this.toReveal = this.props.text.length + this.childrenCount;

        this.skipMap = {
            ' ': 2,
            '.': 40,
            ',': 20,
            '!': 50,
            '?': 50,
            '\n': 60,
        };
    }

    componentDidMount() {
        this.timerID = setInterval(() => this.advance(), this.delay);
    }

    skip() {
        this.setState({ revealed: this.toReveal });
    }

    advance() {
        this.setState(state => {
            if (state.revealed > this.toReveal) {
                clearInterval(this.timerID);
                return;
            }
            if (state.revealed < this.props.text.length) {
                const lastChar = this.props.text[state.revealed - 1];

                if (state.skipped < this.skipMap[lastChar]) {
                    return { skipped: state.skipped + 1 };
                }

                return { revealed: state.revealed + 1, skipped: 0 };
            } else {
                if (state.skipped < 60) {
                    return { skipped: state.skipped + 1 };
                }

                return { revealed: state.revealed + 1, skipped: 0 };
            }

        });
    }

    render() {
        const revealedText = this.props.text.slice(0, this.state.revealed);
        const revealedChildrenIdx = this.state.revealed - this.props.text.length;
        let revealedChildren = <></>;
        if (revealedChildrenIdx > 0) {
            revealedChildren = React.Children.map(this.props.children, (c, i) => i < revealedChildrenIdx ? c : <></>);
        }
        return (
            <div onClick={() => this.skip()}>
                {revealedText.split('\n').map(
                    (line, i) => <p className='type' key={i}>{line}</p>
                )}
                {revealedChildren}
            </div>
        );
    }
}

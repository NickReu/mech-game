import React, { Component } from 'react';

import { context } from 'src/engine/context';
import Typer from 'src/components/typer/typer';
import { actionMap } from '../../engine/actionMap';

export default class Hub extends Component {
    static contextType = context;

    componentDidMount() {
        this.context.set({ you: { status: 'fine' } });
    }

    render() {
        const message = 'you wonder what you should do next.';
        return (
            <Typer key={0} text={message} delay={10}>
                <input className='margin' type='button' value='shop' onClick={() => this.context.set({ scene: 'shop' })} />
                <input className='margin' type='button' value='tinker' onClick={() =>
                    this.context.set({
                        scene: 'hub',
                        you: {
                            health: this.context.s.you.maxHealth,
                        },
                    })
                } />
                <input className='margin' type='button' value='explore' onClick={() =>
                    this.context.set({
                        scene: 'fight',
                        fight: {
                            winStory: {
                                arc: 'victory',
                            },
                            loseStory: {
                                arc: 'recovery',
                            },
                            opp: {
                                actions: Object.values(actionMap),
                                health: 19,
                                maxHealth: 19,
                                maxAgility: 12,
                                difficulty: 50,
                            },
                        },
                        you: {
                            actions: Object.values(actionMap),
                            status: 'determined',
                        }
                    })
                } />
            </Typer>
        );
    }
}

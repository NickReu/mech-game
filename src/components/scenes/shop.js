import React, { Component } from 'react';

import { context } from 'src/engine/context';
import Typer from 'src/components/typer/typer';

export default class Shop extends Component {
    static contextType = context;

    onCompletion() {
        this.context.set({ scene: 'hub' });
    }

    render() {
        const message = 'you shop';
        return (
            <Typer key={0} text={message}>
                <input type='button' value='next' onClick={() => this.onCompletion()} />
            </Typer>
        );
    }
}

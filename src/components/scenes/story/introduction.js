import React, { Component } from 'react';

import { context } from 'src/engine/context';
import { actionMap } from 'src/engine/actionMap';
import Typer from 'src/components/typer/typer';

export default class Introduction extends Component {
    static contextType = context;

    constructor(props) {
        super(props);
        this.state = {
            stage: 0,
        };
    }

    startIntroFight() {
        this.context.set({
            scene: 'fight',
            fight: {
                winStory: {
                    arc: 'arc0',
                    chapter: 'firstFightRecover',
                },
                loseStory: {
                    arc: 'arc0',
                    chapter: 'next',
                },
                opp: {
                    actions: [actionMap.volley],
                    health: 6,
                    maxHealth: 6,
                    maxAgility: 12,
                    difficulty: 0,
                },
            },
            you: {
                status: 'alarmed',
                mech: { name: 'scrappy junker' },
            },
        });
    }

    render() {
        if (this.state.stage === 0) {
            const message = 'you stumble to your feet.\n\
                you are at the bottom of a canyon.\n\
                the floor of the canyon is littered with boulders and scrap metal. some of the scraps are more intact than others.';
            return (
                <Typer key={this.state.stage} text={message}>
                    <input type='button' value='walk' onClick={() => this.setState({ stage: 2 })} />
                </Typer>
            );
        } else if (this.state.stage === 2) {
            const message = 'you walk along for a bit.\n\
                it\'s quiet in the canyon, besides the occasional gust of wind.\n\
                you try to remember how you got here.\n\
                you can\'t seem to.\n\
                it looks like the rocks and scraps must have fallen down from the cliffs.\n\
                you wonder what happened up there, to send such quantities of material over the edge. is it still happening?\n\
                presently, you find a machine that looks almost intact.\n\
                it\'s some sort of mech or battlesuit, with legs, a cockpit, and guns mounted to the sides.\n\
                it seems familiar to you somehow...';
            return (
                <Typer key={this.state.stage} text={message}>
                    <input type='button' value='climb in' onClick={() => this.setState({ stage: 10 })} />
                </Typer>
            );
        } else if (this.state.stage === 10) {
            const message = 'the inside is cramped, but not uncomfortable. you\'re glad to learn you aren\'t claustrophobic.\n\
                you begin to fiddle with the controls. there are pedals beneath your feet that control movement, and buttons aplenty at your fingertips.\n\
                a screen flickers to life in front of your eyes, highlighting elements beyond the windshield with numbers and labels.\n\
                this thing must have been built tough to survive falling down the canyon.\n\n\
                just as you start to learn how to work the machine, you see movement behind a distant boulder.\n\
                you can\'t make out exactly what is moving, but it is vaguely shaped like a person.\n\n\
                BOOM!\n\
                a small missile explodes near you. the distant figure must be in a machine of their own!\n\
                you prepare to engage.';
            return (
                <Typer key={this.state.stage} text={message}>
                    <input type='button' value='fight!' onClick={() => this.startIntroFight()} />
                </Typer>
            );
        }
    }
}

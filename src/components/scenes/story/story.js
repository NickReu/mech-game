import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { context } from 'src/engine/context';
import Introduction from './introduction';
import Arc0 from './arc0';
import Victory from './victory';
import Recovery from './recovery';

export default class SceneSwitcher extends Component {
    static contextType = context;
    static propTypes = {
        arc: PropTypes.string.isRequired,
        chapter: PropTypes.string,
        part: PropTypes.string,
    };

    componentDidUpdate(prevProps) {
        if (this.props.arc !== prevProps.arc ||
            this.props.chapter !== prevProps.chapter ||
            this.props.part !== prevProps.part) {
            this.context.save();
        }
    }

    render() {
        if (this.props.arc === 'intro') {
            return <Introduction />;
        } else if (this.props.arc === 'arc0') {
            return <Arc0 chapter={this.props.chapter} />;
        } else if (this.props.arc === 'victory') {
            return <Victory />;
        } else if (this.props.arc === 'recovery') {
            return <Recovery />;
        }
    }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { context } from 'src/engine/context';
import AfterFirstFight from './afterFirstFight';

export default class Arc0 extends Component {
    static contextType = context;
    static propTypes = {
        chapter: PropTypes.string.isRequired,
    };

    componentDidMount() {
        this.context.set({
            you: { status: 'alert' }
        });
    }

    render() {
        if (this.props.chapter === 'firstFightRecover') {
            return <AfterFirstFight />;
        } else if (this.props.chapter === 'next') {
            return <p>next</p>;
        }
    }
}

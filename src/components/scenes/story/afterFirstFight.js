import React, { Component } from 'react';

import { context } from 'src/engine/context';
import Typer from 'src/components/typer/typer';

export default class AfterFirstFight extends Component {
    static contextType = context;

    constructor(props) {
        super(props);
        this.state = {
            stage: 0,
        };
    }

    onCompletion() {
        this.context.set({ scene: 'hub' });
    }

    render() {
        if (this.state.stage === 0) {
            const message = 'you\'re relieved as the other mech explodes, though you realize you escaped by luck more than strategy.\n\
                your mech just managed to take a beating better.\n\
                maybe your unknown opponent found their mech down here, just like you.\n\
                you wish you could remember why you\'re here, or what you were doing. \n\n   or anything, really.\n\
                unfortunately there\'s no time to sit and ponder. this canyon is more dangerous than you\'d realized.\n\
                you should probably try to find a way out. the mech should help you cover more distance.';
            return (
                <Typer key={this.state.stage} text={message}>
                    <input type='button' value='keep going' onClick={() => this.setState({ stage: 2 })} />
                </Typer>
            );
        } else if (this.state.stage === 2) {
            const message = 'as you continue to follow the canyon\'s path, you become more comfortable piloting the mech.\n\
                after passing lots more broken junk and fallen rocks, you come upon a set of crumbling stairs that lead up the side of the cliff.';
            return (
                <Typer key={this.state.stage} text={message}>
                    <input className='margin' type='button' value='climb the stairs' onClick={() => this.setState({ stage: 3 })} />
                    <input className='margin' type='button' value='continue down the canyon' onClick={() => this.setState({ stage: 10 })} />
                </Typer>
            );
        } else if (this.state.stage === 3) {
            const message = 'stairs';
            return (
                <Typer key={this.state.stage} text={message}>
                    <input type='button' value='next' onClick={() => this.setState({ stage: 10 })} />
                </Typer>
            );
        } else if (this.state.stage === 10) {
            const message = 'you manage to escape the canyon.';
            return (
                <Typer key={this.state.stage} text={message}>
                    <input type='button' value='next' onClick={() => this.onCompletion()} />
                </Typer>
            );
        }
    }
}

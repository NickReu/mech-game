import React, { Component } from 'react';

import { context } from 'src/engine/context';
import Typer from 'src/components/typer/typer';

export default class Victory extends Component {
    static contextType = context;

    onCompletion() {
        this.context.set({ scene: 'hub' });
    }

    render() {
        const message = 'you take some deep breaths to calm down after the fight.';
        return (
            <Typer key={0} text={message}>
                <input className='margin' type='button' value='next' onClick={() => this.onCompletion()} />
            </Typer>
        );
    }
}

import React, { Component } from 'react';

import { context } from 'src/engine/context';
import Typer from 'src/components/typer/typer';

export default class Recovery extends Component {
    static contextType = context;

    onCompletion() {
        this.context.set({
            scene: 'hub',
            you: {
                health: this.context.s.you.maxHealth,
            },
        });
    }

    render() {
        const message = 'you flee! your mech is severely damaged.';
        return (
            <Typer key={0} text={message}>
                <input className='margin' type='button' value='repair' onClick={() => this.onCompletion()} />
            </Typer>
        );
    }
}

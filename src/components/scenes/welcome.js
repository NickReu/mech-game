import React, { Component } from 'react';
import Typer from '../typer/typer';
import { context } from '../../engine/context';

export default class Welcome extends Component {
    static contextType = context;

    constructor(props) {
        super(props);
        this.state = {
            nameValue: '',
            stage: 'blank',
        };
    }

    componentDidMount() {
        const saved = window.localStorage.getItem('save0');
        if (saved != undefined) {
            try {
                const saveData = JSON.parse(saved);
                if (!saveData.you.name) {
                    throw new Error('name not defined');
                }
                this.setState({ stage: 'recognized', nameValue: saveData.you.name });
                this.saveData = saveData;
            } catch {
                this.setState({ stage: 'askName' });
            }
        } else {
            this.setState({ stage: 'askName' });
        }
    }

    handleChangeName(event) { this.setState({ nameValue: event.target.value }); }

    handleSubmitName(event) {
        event.preventDefault();
        if (this.state.nameValue.length === 0) { return; }
        this.setState({ stage: 'confirmName' });
    }

    saveName() {
        this.context.set({ you: { name: this.state.nameValue } });
        this.context.set({ scene: 'story', story: { arc: 'intro' } });
    }

    render() {
        if (this.state.stage === 'blank') {
            return <></>;
        } else if (this.state.stage === 'askName') {
            const message = 'you awaken. what is your name?';
            return (
                <div>
                    <Typer key={0} text={message} delay={35} >
                        <form onSubmit={event => this.handleSubmitName(event)}>
                            <input className='margin' type="text" value={this.state.nameValue} onChange={event => this.handleChangeName(event)} />
                            <input className='margin' type="submit" value="next" />
                        </form>
                    </Typer>
                </div>
            );
        } else if (this.state.stage === 'confirmName') {
            const message = `you recall your name is ${this.state.nameValue}.`;
            return (
                <div>
                    <Typer key={1} text={message} >
                        <input className='margin' type="button" value="yes" onClick={() => this.saveName()} />
                        <input className='margin' type="button" value="no" onClick={() => this.setState({ stage: 'askName' })} />
                    </Typer>
                </div>
            );
        } else if (this.state.stage === 'recognized') {
            return (
                <div>
                    <Typer key={2} text={`welcome back, ${this.state.nameValue}!`} >
                        <input className='margin' type='button' value='next' onClick={() => this.context.set(this.saveData)} />
                    </Typer>
                </div>
            );
        }
    }
}

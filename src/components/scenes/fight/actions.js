import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Typer from 'src/components/typer/typer';

import './styles/actions.css';

export class ActionList extends Component {
    static propTypes = {
        showRange: PropTypes.func.isRequired,
        choose: PropTypes.func.isRequired,
        actions: PropTypes.array.isRequired,
    };

    render() {
        return (
            <div className='actionChooser'>
                <Typer text='choose an action:' />
                <div className='actionList'>
                    {this.props.actions.map(a =>
                        <button
                            key={a.name}
                            type='button'
                            className='actionButton'
                            onMouseEnter={() => this.props.showRange(a)}
                            onMouseLeave={() => this.props.showRange()}
                            onClick={() => this.props.choose(a)}
                        >
                            <ActionContent a={a} />
                        </button>
                    )}
                </div>
            </div>
        );
    }
}

export class ActionCard extends Component {
    static propTypes = {
        a: PropTypes.object.isRequired,
        hidden: PropTypes.bool,
    };

    render() {
        return (
            <div className='actionCard' style={{ color: this.props.hidden ? 'white' : 'unset', borderColor: this.props.hidden ? 'white' : 'unset' }}>
                <ActionContent a={this.props.a} />
            </div>
        );
    }

}

class ActionContent extends Component {
    static propTypes = {
        a: PropTypes.object.isRequired,
    };

    render() {
        const a = this.props.a;
        let rangeString = '';
        if (a.range[0] >= a.range[1]) {
            rangeString = a.range[0].toString();
        } else {
            rangeString = `${a.range[0]}-${a.range[1]}`;
        }

        return (
            <div className='action'>
                <h1>{a.name}</h1>
                <table>
                    <tbody>
                        <tr>
                            <td key={0}>speed</td>
                            <td key={1}>{a.speed}</td>
                        </tr>
                        <tr>
                            <td key={10}>range</td>
                            <td key={11}>{rangeString}</td>
                        </tr>
                        <tr>
                            <td key={20}>damage</td>
                            <td key={21}>{a.damage}</td>
                        </tr>
                        <tr>
                            <td key={30}>persist</td>
                            <td key={31}>{a.persist}</td>
                        </tr>
                    </tbody>
                </table>
                <div className='actionPhases'>
                    <dl>
                        {/* {a.sot?.s && <><dt>start of turn:</dt><dd>{a.sot.s}</dd></>} */}
                        {a.pa?.s && <><dt>pre-action:</dt><dd>{a.pa.s}</dd></>}
                        {a.eot?.s && <><dt>end of turn:</dt><dd>{a.eot.s}</dd></>}
                    </dl>
                </div>
                <div className='actionText'>
                    {a.extra && a.extra.split('\n').map(line =>
                        <p key={line}>{line}</p>
                    )}
                </div>
            </div>
        );
    }
}

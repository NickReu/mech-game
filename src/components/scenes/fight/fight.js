import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { context } from 'src/engine/context';
import * as cb from 'src/engine/combat';

import Typer from 'src/components/typer/typer';
import { ActionList, ActionCard } from './actions';
import FightField from './fightField';
import BattleLog from './battleLog';
import PillBar from './pillBar';

import './styles/fight.css';
import { chooseAction } from 'src/engine/ai';
import { actionMap } from 'src/engine/actionMap';

const FieldLength = 6;
const YourStartingZone = 1;
const OppStartingZone = 4;

export default class Fight extends Component {
    static contextType = context;
    static propTypes = {
        you: PropTypes.shape({
            actions: PropTypes.array.isRequired,
            health: PropTypes.number.isRequired,
            maxHealth: PropTypes.number.isRequired,
            maxAgility: PropTypes.number.isRequired,
        }).isRequired,
        opp: PropTypes.shape({
            actions: PropTypes.array.isRequired,
            health: PropTypes.number.isRequired,
            maxHealth: PropTypes.number.isRequired,
            maxAgility: PropTypes.number.isRequired,
            difficulty: PropTypes.number.isRequired,
        }).isRequired,
    };

    constructor(props) {
        super(props);

        this.fieldLength = FieldLength;

        this.state = {
            phase: 'choose',
            turn: 0,
            history: [new cb.InitialUpdate(FieldLength, YourStartingZone, OppStartingZone, this.props.you.health, this.props.opp.health)],
            hl: [null, null, null, null, null, null],
            tooltipAction: null,
            you: {
                ...this.props.you,
                loc: YourStartingZone,
                agility: 0,
            },
            opp: {
                ...this.props.opp,
                loc: OppStartingZone,
                name: 'the enemy',
                agility: 0,
            },
            currentAction: {},
            currentOppAction: {},
        };
    }

    componentDidUpdate(_prevProps, prevState) {
        if (prevState.phase === 'choose' && this.state.phase === 'preAction') {
            this.doPreAction(this.state.you.currentAction, this.state.opp.currentAction);
        } else if (prevState.phase === 'preAction' && this.state.phase === 'hit') {
            this.doHit(this.state.you.currentAction, this.state.opp.currentAction);
        } else if (prevState.phase === 'hit' && this.state.phase === 'endOfTurn') {
            this.doEndOfTurn(this.state.you.currentAction, this.state.opp.currentAction);
        } else if (prevState.phase === 'endOfTurn' && this.state.phase === 'choose') {
            this.checkWin();
        }
    }

    appendHistory(toAdd) {
        if (typeof toAdd === 'string' || toAdd instanceof cb.Update) {
            toAdd = [toAdd];
        }
        this.setState(state => {
            return { history: [...state.history, ...toAdd] };
        });
    }

    fulfillUpdate(update) {
        if (update instanceof cb.MoveUpdate) {
            this.doMove(update.actor, update.amount);
        } else if (update instanceof cb.ShoveUpdate) {
            this.doMove(update.target, -1 * update.amount);
        } else if (update instanceof cb.HitUpdate) {
            this.doDamage(update.target, update.amount);
        } else if (update instanceof cb.AgilityUpdate) {
            this.addAgility(update.actor, update.amount);
        } else if (update instanceof cb.MissUpdate) {
            // pass
        } else if (update instanceof cb.StaggerUpdate) {
            // pass
        } else {
            console.log(update.kind.kind);
        }
        this.appendHistory(update);
    }

    processUpdates(updates) {
        updates.forEach(update => { this.fulfillUpdate(update); });
    }

    nextTurn() {
        this.setState(state => {
            return {
                history: [...state.history, `turn ${state.turn + 1} begins.`],
                turn: state.turn + 1
            };
        });
    }

    makeChoice(choice) {
        this.nextTurn();
        let nextChoices = this.props.you.actions;
        if (nextChoices.length > 1) {
            nextChoices = nextChoices.filter(a => a.name != choice.name);
        }
        const oppChoice = chooseAction(
            this.state.history,
            cb.Actor.Opp,
            this.state.opp.actions,
            this.state.opp.currentAction,
            cb.Actor.You,
            this.state.you.actions,
            this.state.you.currentAction,
            this.state.opp.difficulty,
        );
        this.appendHistory(new cb.ChooseUpdate(cb.Actor.You, choice));
        this.appendHistory(new cb.ChooseUpdate(cb.Actor.Opp, oppChoice));
        this.setState(state => ({
            phase: 'preAction',
            you: {
                ...state.you,
                currentAction: choice,
                actions: nextChoices,
            },
            opp: {
                ...state.opp,
                currentAction: oppChoice,
            },
        }));
    }

    doPreAction(action, oppAction) {
        this.processUpdates(cb.getPreActionUpdates(action, oppAction));
        this.setState({ phase: 'hit' });
    }

    doHit(action, oppAction) {
        this.processUpdates(cb.getHitUpdates(this.state.history, action, oppAction));
        this.setState({ phase: 'endOfTurn' });
    }

    doEndOfTurn(action, oppAction) {
        this.processUpdates(cb.getEndOfTurnUpdates(action, oppAction));
        this.setState({ phase: 'choose' });
    }

    checkWin() {
        if (this.state.you.health === 0) {
            this.setState({ phase: 'done', wonFight: false });
        } else if (this.state.opp.health === 0) {
            this.setState({ phase: 'done', wonFight: true });
        }
    }

    doDamage(hurt, damage) {
        this.setState(state => {
            return {
                [hurt.who]: {
                    ...state[hurt.who],
                    health: Math.max(0, state[hurt.who].health - damage),
                }
            };
        });
    }

    addAgility(target, amount) {
        this.setState(state => {
            return {
                [target.who]: {
                    ...state[target.who],
                    agility: Math.min(Math.max(0, state[target.who].agility + amount), 12),
                }
            };
        });
    }

    doMove(mover, displacement) {
        this.setState(state => {
            return {
                [mover.who]: {
                    ...state[mover.who],
                    loc: cb.getMoveResult(state.history,
                        mover,
                        displacement,
                        state[mover.who].loc,
                        state[mover.other().who].loc,
                        state[this.fieldLength],
                    ),
                }
            };
        });
    }

    showRange(action) {
        const hl = [null, null, null, null, null, null];
        const range = action?.range;
        this.setState(state => {
            if (range != undefined) {
                const simState = [...state.history];
                if (action.pa?.move) {
                    simState.push(new cb.MoveUpdate(cb.Actor.You, action.pa.move));
                }
                if (action.pa?.shove) {
                    simState.push(new cb.ShoveUpdate(cb.Actor.You, cb.Actor.Opp, action.pa.shove));
                }
                const targets = cb.getTargetedSquares(simState, cb.Actor.You, range, this.fieldLength);
                for (const target of targets) {
                    hl[target] = 'lightCoral';
                }
            }
            return { hl: hl };
        });
    }

    showActionDetails(action) {
        if (action) {
            this.setState({ tooltipAction: action });
        } else {
            this.setState({ tooltipAction: null });
        }
    }

    resolve() {
        const targetStory = this.state.wonFight ? this.context.s.fight.winStory : this.context.s.fight.loseStory;
        this.context.set({ scene: 'story', story: targetStory, you: { health: this.state.you.health } });
    }

    render() {
        let contents = <></>;
        if (this.state.phase === 'choose') {
            contents = <ActionList
                actions={this.state.you.actions}
                choose={choice => this.makeChoice(choice)}
                showRange={range => this.showRange(range)}
            />;
        } else if (this.state.phase === 'done') {
            const message = this.state.wonFight ? 'you won!' : 'you lost.';
            contents = <>
                <Typer text={message} />
                <input type='button' value='next' onClick={() => this.resolve()} />
            </>;
        }
        return (
            <div className='fightBox'>
                <div className='fightBoxMain'>
                    <PillBar color='lightCoral' label='your health' max={this.props.you.maxHealth} value={this.state.you.health} />
                    <PillBar color='skyBlue' label='your agility' max={this.props.you.maxAgility} value={this.state.you.agility} />
                    <FightField you={this.state.you.loc} opp={this.state.opp.loc} hl={this.state.hl} />
                    <PillBar color='moccasin' label='enemy health' max={this.props.opp.maxHealth} value={this.state.opp.health} reversed />
                    <PillBar color='mediumAquamarine' label='enemy agility' max={this.props.opp.maxAgility} value={this.state.opp.agility} reversed />
                    {contents}
                </div>
                <BattleLog history={this.state.history} onTooltip={a => this.showActionDetails(a)} />
                {this.state.tooltipAction ? <ActionCard a={this.state.tooltipAction} /> : <ActionCard a={actionMap.lunge} hidden />}
            </div>
        );
    }
}

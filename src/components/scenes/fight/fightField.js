import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './styles/fightField.css';

export default class FightField extends Component {
    static propTypes = {
        you: PropTypes.number,
        opp: PropTypes.number,
        hl: PropTypes.array
    };

    render() {
        const zones = [];
        for (let i = 0; i < 6; i++) {
            let inhabitant = '';
            const style = {};
            if (i == this.props.you) {
                inhabitant = 'you';
            } else if (i == this.props.opp) {
                inhabitant = 'enemy';
            }
            style.backgroundColor = this.props.hl[i];
            zones.push(
                <div key={i} className='zone' style={style}>
                    {inhabitant}
                </div>
            );
        }
        return (
            <div className='fightField'>
                {zones}
            </div>
        );
    }
}

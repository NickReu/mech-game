import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './styles/battleLog.css';
import { ChooseUpdate } from 'src/engine/combat';

export default class BattleLog extends Component {
    static propTypes = {
        history: PropTypes.array.isRequired,
        onTooltip: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.bottom = React.createRef();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.history.length < this.props.history.length) {
            this.bottom.current.scrollIntoView(false);
        }
    }

    historyToP(item, index) {
        if (item instanceof ChooseUpdate) {
            return (
                <p
                    key={index}
                    onMouseEnter={() => this.props.onTooltip(item.action)}
                    onMouseLeave={() => this.props.onTooltip()}
                >
                    {item.toString()}
                </p>
            );
        }
        return <p key={index}>{item.toString()}</p>;
    }

    render() {
        return (
            <div className='battleLog' style={{ display: this.props.history.filter(l => l.length).length ? 'flex' : 'none', }}>
                <h1>battle log</h1>
                <div className='battleLogList'>
                    {this.props.history.map((e, i) => this.historyToP(e, i))}
                    <div key='bottom' ref={this.bottom} />
                </div>
            </div>
        );
    }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './styles/pillBar.css';

export default class PillBar extends Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        max: PropTypes.number.isRequired,
        value: PropTypes.number.isRequired,
        reversed: PropTypes.bool,
    };

    render() {
        const max = this.props.max;
        const value = this.props.value;
        const color = this.props.value > 0 ? this.props.color : 'transparent';
        return (
            <div className='pillBarContainer' style={{ alignSelf: this.props.reversed ? 'flex-end' : 'flex-start' }}>
                <p style={{ textAlign: this.props.reversed ? 'right' : 'left' }}> {this.props.label} </p>
                <div className='pillBar' style={{ width: `${max}em` }}>
                    <div className='pillBarValue' style={{
                        width: `${value}em`,
                        backgroundColor: color, borderColor: color,
                        left: this.props.reversed ? 1 : -1,
                        float: this.props.reversed ? 'right' : 'left',
                    }}>
                        <div>{`${value}/${max}`}</div>
                    </div>
                </div>
            </div>
        );
    }
}

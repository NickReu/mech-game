import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { context } from 'src/engine/context';
import Welcome from './welcome';
import Story from './story/story';
import Fight from './fight/fight';
import Shop from './shop';
import Hub from './hub';

export default class SceneSwitcher extends Component {
    static contextType = context;
    static propTypes = {
        scene: PropTypes.string,
    };

    componentDidUpdate(prevProps) {
        if (this.props.scene !== prevProps.scene) {
            this.context.save();
        }
    }

    render() {
        let contents = <></>;
        if (this.props.scene === 'default') {
            contents = <Welcome />;
        } else if (this.props.scene === 'story') {
            contents = <Story
                arc={this.context.s.story.arc}
                chapter={this.context.s.story.chapter}
                part={this.context.s.story.part}
            />;
        } else if (this.props.scene === 'fight') {
            contents = <Fight
                you={this.context.s.you}
                opp={this.context.s.fight.opp}
            />;
        } else if (this.props.scene === 'shop') {
            contents = <Shop />;
        } else if (this.props.scene === 'hub') {
            contents = <Hub />;
        }
        return (
            <div className='playColumn mainColumn' >
                {contents}
            </div>
        );
    }
}

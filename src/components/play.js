import React, { Component } from 'react';

import { context } from 'src/engine/context';
import Summary from './summary';
import SceneSwitcher from './scenes/sceneSwitcher';

export default class Play extends Component {
    static contextType = context;

    render() {
        return (
            <div className='play'>
                <Summary />
                <SceneSwitcher scene={this.context.s.scene} />
            </div>
        );
    }
}

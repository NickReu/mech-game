import React, { Component } from 'react';
import { context } from 'src/engine/context';

export default class Footer extends Component {
    static contextType = context;

    restart() {
        if (confirm('are you sure? this will reset all progress.')) {
            window.localStorage.clear();
            window.location.assign(window.location);
        }
    }

    render() {
        return (
            <div className="footer">
                <div className='footerButtons'>
                    <input className='margin' type='button' value='save' onClick={this.context.save} />
                    <input className='margin' type='button' value='forget' onClick={this.restart} />
                </div>
                <br />
                <div className='footer2'>
                    <div className='faded version'>
                        v{NPM_PACKAGE_VERSION}
                    </div>
                    <div className='faded'>
                        <a href='https://gitlab.com/NickReu/mech-game' target='_blank' rel="noreferrer">src</a>
                    </div>
                </div>
            </div>

        );
    }
}

import { s } from 'src/utils/text';

export class Actor {
    static You = new Actor('you');
    static Opp = new Actor('opp');

    constructor(who) {
        this.who = who;
    }

    noun(oppName) {
        if (oppName == undefined) {
            oppName = 'the enemy';
        }
        return this.who === 'opp' ? oppName : 'you';
    }

    is() {
        return this.who === 'opp' ? 'is' : 'are';
    }

    was() {
        return this.who === 'opp' ? 'was' : 'were';
    }

    s() {
        return this.who === 'opp' ? 's' : '';
    }

    es() {
        return this.who === 'opp' ? 'es' : '';
    }

    other() {
        return this.who === 'you' ? Actor.Opp : Actor.You;
    }
}

export class Update {
    constructor(kind) {
        this.kind = kind;
        this.isUpdate = true;
    }
}

class UpdateKind {
    static Initial = new UpdateKind('initial');
    static Move = new UpdateKind('move');
    static Shove = new UpdateKind('shove');
    static Hit = new UpdateKind('hit');
    static Miss = new UpdateKind('miss');
    static Stagger = new UpdateKind('stagger');
    static Choose = new UpdateKind('choose');
    static Agility = new UpdateKind('agility');
    constructor(kind) {
        this.kind = kind;
    }
}

export class InitialUpdate extends Update {
    constructor(fieldLength, youLoc, oppLoc, youHealth, oppHealth) {
        super(UpdateKind.Initial);
        this.fieldLength = fieldLength;
        this.youLoc = youLoc;
        this.oppLoc = oppLoc;
        this.youHealth = youHealth;
        this.oppHealth = oppHealth;
    }

    toString() {
        return '';
    }
}

export class MoveUpdate extends Update {
    constructor(actor, amount) {
        super(UpdateKind.Move);
        this.actor = actor;
        this.amount = amount;
    }

    toString() {
        const direction = this.amount > 0 ? 'forward' : 'backward';
        return `${this.actor.noun()} move${this.actor.s()} ${Math.abs(this.amount)} zone${s(this.amount)} ${direction}.`;
    }
}

export class ShoveUpdate extends Update {
    constructor(actor, target, amount) {
        super(UpdateKind.Shove);
        this.actor = actor;
        this.target = target;
        this.amount = amount;
    }

    toString() {
        const verb = this.amount > 0 ? 'shove' : 'yank';
        const direction = this.amount > 0 ? 'away' : 'closer';
        return `${this.actor.noun()} ${verb}${this.actor.s()} ${this.target.noun()} ${Math.abs(this.amount)} zone${s(this.amount)} ${direction}.`;
    }
}

export class HitUpdate extends Update {
    constructor(actor, target, amount) {
        super(UpdateKind.Hit);
        this.actor = actor;
        this.target = target;
        this.amount = amount;
    }

    toString() {
        if (this.target !== this.actor) {
            return `${this.actor.noun()} hit${this.actor.s()} ${this.target.noun()} for ${this.amount} damage!`;
        } else {
            return `${this.actor.noun()} hit${this.actor.s()} themself for ${this.amount} damage!`;
        }
    }
}

export class StaggerUpdate extends Update {
    constructor(target, persist) {
        super(UpdateKind.Stagger);
        this.target = target;
        this.persist = persist;
    }

    toString() {
        return `${this.target.noun()} (persist ${this.persist}) ${this.target.is()} staggered and need${this.target.s()} to recover.`;
    }
}

export class MissUpdate extends Update {
    constructor(actor) {
        super(UpdateKind.Miss);
        this.actor = actor;
    }

    toString() {
        return `${this.actor.noun()} miss${this.actor.es()}.`;
    }
}

export class ChooseUpdate extends Update {
    constructor(actor, action) {
        super(UpdateKind.Choose);
        this.actor = actor;
        this.action = action;
    }

    toString() {
        return `${this.actor.noun()} prepare${this.actor.s()} to ${this.action.name} (speed ${this.action.speed}).`;
    }
}

export class AgilityUpdate extends Update {
    constructor(actor, amount) {
        super(UpdateKind.Choose);
        this.actor = actor;
        this.amount = amount;
    }

    toString() {
        const verb = this.amount > 0 ? 'gain' : 'lose';
        return `${this.actor.noun()} ${verb}${this.actor.s()} ${this.amount} agility.`;
    }
}


export function getTriggeredUpdates(action, oppAction, phase) {
    const youFirst = action.speed > oppAction.speed;
    const yourMove = action[phase]?.move ? [new MoveUpdate(Actor.You, action[phase].move)] : [];
    const oppMove = oppAction[phase]?.move ? [new MoveUpdate(Actor.Opp, oppAction[phase].move)] : [];
    const yourShove = action[phase]?.shove ? [new ShoveUpdate(Actor.You, Actor.Opp, action[phase].shove)] : [];
    const oppShove = oppAction[phase]?.shove ? [new ShoveUpdate(Actor.Opp, Actor.You, oppAction[phase].shove)] : [];
    const yourAgility = action[phase]?.agility ? [new AgilityUpdate(Actor.You, action[phase].agility)] : [];
    const oppAgility = oppAction[phase]?.agility ? [new AgilityUpdate(Actor.Opp, oppAction[phase].agility)] : [];
    const yourUpdates = [...yourMove, ...yourShove, ...yourAgility];
    const oppUpdates = [...oppMove, ...oppShove, ...oppAgility];
    return youFirst ? [...yourUpdates, ...oppUpdates] : [...oppUpdates, ...yourUpdates];
}

export function getPreActionUpdates(action, oppAction) {
    return getTriggeredUpdates(action, oppAction, 'pa');
}

export function getEndOfTurnUpdates(action, oppAction) {
    return getTriggeredUpdates(action, oppAction, 'eot');
}

export function getHalfHitUpdates(state, actor, target, action, targetAction, first) {
    const happenings = [];
    const targets = getTargetedSquares(state, actor, action.range);
    let staggered = false;
    if (targets.length > 0) {
        if (targets.includes(getLoc(state, target))) {
            happenings.push(new HitUpdate(actor, target, action.damage));
            if (first && action.damage >= targetAction.persist) {
                happenings.push(new StaggerUpdate(target, targetAction.persist));
                staggered = true;
            }
        } else {
            happenings.push(new MissUpdate(actor));
        }
    }
    return [happenings, staggered];
}

export function getHitUpdates(state, yourAction, oppAction) {
    const happenings = [];
    const youFirst = yourAction.speed > oppAction.speed;
    const firstActor = youFirst ? Actor.You : Actor.Opp;
    const secondActor = youFirst ? Actor.Opp : Actor.You;
    const firstAction = youFirst ? yourAction : oppAction;
    const secondAction = youFirst ? oppAction : yourAction;

    const [firstUpdates, staggered] = getHalfHitUpdates([...state, ...happenings], firstActor, secondActor, firstAction, secondAction, true);
    happenings.push(...firstUpdates);
    if (!staggered) {
        const [secondUpdates, _] = getHalfHitUpdates([...state, ...happenings], secondActor, firstActor, secondAction, firstAction, false);
        happenings.push(...secondUpdates);
    }
    return happenings;
}

export function getTargetedSquares(state, attacker, range, _fieldLength) {
    const loc = getLoc(state, attacker);
    const otherLoc = getLoc(state, attacker.other());
    const fieldLength = _fieldLength ?? getFieldLength(state);

    const targets = [];
    for (let i = range[0]; i <= range[1]; i++) {
        const target = otherLoc > loc ? loc + i : loc - i;
        if (target < 0 || target >= fieldLength) { continue; }
        targets.push(target);
    }
    return targets;
}

export function getLoc(state, target) {
    const locs = {
        [Actor.You.who]: 1,
        [Actor.Opp.who]: 4,
    };
    let fieldLength = 6;
    for (const update of state) {
        if (!update.isUpdate) { continue; }
        if (update instanceof MoveUpdate) {
            locs[update.actor.who] = getMoveResult([], update.actor, update.amount, locs[update.actor.who], locs[update.actor.other().who], fieldLength);
        } else if (update instanceof ShoveUpdate) {
            locs[update.target.who] = getMoveResult([], update.target, -1 * update.amount, locs[update.target.who], locs[update.actor.who], fieldLength);
        } else if (update instanceof InitialUpdate) {
            locs[Actor.You.who] = update.youLoc;
            locs[Actor.Opp.who] = update.oppLoc;
            fieldLength = update.fieldLength;
        }
    }
    if (target) {
        return locs[target.who];
    } else {
        return locs;
    }
}

export function getFieldLength(state) {
    let fieldLength = 6;
    for (const update of state) {
        if (update instanceof InitialUpdate) {
            fieldLength = update.fieldLength;
        }
    }
    return fieldLength;
}

export function getMoveResult(state, mover, displacement, _moverLoc, _otherLoc, _fieldLength) {
    const oldLoc = _moverLoc ?? getLoc(state, mover);
    const otherLoc = _otherLoc ?? getLoc(state, mover.other());
    const fieldLength = _fieldLength ?? getFieldLength(state);
    const direction = Math.sign(displacement) * (oldLoc < otherLoc ? 1 : -1);
    const distance = Math.abs(displacement);
    let newLoc = oldLoc + distance * direction;
    const skippedRight = newLoc >= otherLoc && oldLoc < otherLoc;
    const skippedLeft = newLoc <= otherLoc && oldLoc > otherLoc;
    if (skippedRight || skippedLeft) {
        newLoc += direction;
    }
    for (let i = 0; i < distance + 1; i++) {
        if (newLoc < 0 || newLoc >= fieldLength || newLoc === otherLoc) {
            newLoc -= direction;
        } else {
            break;
        }
    }
    return newLoc;
}

export function getHealths(state) {
    const health = {
        [Actor.You.who]: Infinity,
        [Actor.Opp.who]: Infinity,
    };
    for (const update of state) {
        if (!update.isUpdate) { continue; }
        if (update instanceof HitUpdate) {
            health[update.target.who] -= update.amount;
        } else if (update instanceof InitialUpdate) {
            health[Actor.You.who] = update.youHealth;
            health[Actor.Opp.who] = update.oppHealth;
        }
    }
    return health;
}

export function getHealthAdvantage(state, actor) {
    const health = getHealths(state);
    return health[actor.who] - health[actor.other().who];
}

export function getWinner(state) {
    const health = getHealths(state);
    if (health[Actor.You.who] <= 0) {
        return Actor.Opp;
    } else if (health[Actor.Opp.who] <= 0) {
        return Actor.You;
    }
}

import { sample } from 'src/utils/arrays';
import { getEndOfTurnUpdates, getHitUpdates, getPreActionUpdates, getWinner, getHealthAdvantage } from './combat';

export function chooseAction(state, actor, actionDeck, lastAction, other, otherActionDeck, otherLastAction, difficulty) {
    if (Math.random() * 100 > difficulty) {
        const allowed = actionDeck.length > 1 ? actionDeck.filter(a => a !== lastAction) : actionDeck;
        return sample(allowed);
    }
    const results = minimax(state, actor, actionDeck, lastAction, other, otherActionDeck, otherLastAction, 2);
    return results.action;
}

function minimax(state, actor, actionDeck, lastAction, other, otherActionDeck, otherLastAction, depth) {
    const winner = getWinner(state);
    if (winner === actor) {
        return { value: 10000 };
    } else if (winner === other) {
        return { value: -10000 };
    }

    if (depth === 0) {
        return { value: getHealthAdvantage(state, actor) };
    }

    let bestAction;
    let expected;
    let bestWorst = -Infinity;
    for (const action of actionDeck) {
        if (action === lastAction) { continue; }
        let worstOtherAction;
        let worstValue = Infinity;
        for (const otherAction of otherActionDeck) {
            if (otherAction == otherLastAction) { continue; }
            const happenings = [];
            happenings.push(...getPreActionUpdates(otherAction, action));
            happenings.push(...getHitUpdates([...state, ...happenings], otherAction, action));
            happenings.push(...getEndOfTurnUpdates(otherAction, action));
            const value = minimax([...state, ...happenings], actor, actionDeck, action, other, otherActionDeck, otherAction, depth - 1).value;
            if (value < worstValue) {
                worstValue = value;
                worstOtherAction = new Set();
                worstOtherAction.add(otherAction.name);
            } else if (value === worstValue) {
                worstOtherAction.add(otherAction.name);
            }
        }
        if (worstValue > bestWorst) {
            bestAction = new Set();
            bestAction.add(action);
            bestWorst = worstValue;
            expected = worstOtherAction;
        } else if (worstValue === bestWorst) {
            bestAction.add(action);
            worstOtherAction.forEach(a => expected.add(a));
        }
    }
    return {
        action: sample(Array.from(bestAction)),
        considered: Array.from(bestAction).map(a => a.name),
        expected: Array.from(expected),
        value: bestWorst
    };
}

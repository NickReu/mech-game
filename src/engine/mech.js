export default class Mech {
    constructor(name) {
        this.name = name;
    }

    static random() {
        return new Mech('random name');
    }
}

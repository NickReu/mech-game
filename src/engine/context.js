import React from 'react';
import { actionMap } from './actionMap';

export const initialState = {
    you: {
        name: localStorage.getItem('id'),
        status: 'confused',
        actions: [actionMap.volley],
        health: 12,
        maxHealth: 21,
        maxAgility: 12,
    },
    scene: 'default',
    story: {
        arc: 'intro',
        chapter: '',
        part: '',
    }
};

export const context = React.createContext({ s: initialState, set: () => { }, save: () => { } });

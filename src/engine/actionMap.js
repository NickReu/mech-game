export const actionMap = {
    swing: {
        name: 'swing', speed: 3, damage: 5, persist: 4, range: [1, 1],
        eot: {
            s: 'shove 1',
            shove: 1,
        },
    },
    lunge: {
        name: 'lunge', speed: 4, damage: 3, persist: 3, range: [1, 1],
        pa: {
            s: 'forwards 1, yank 1',
            move: 1,
            shove: -1,
        },
    },
    volley: {
        name: 'volley', speed: 2, damage: 2, persist: 4, range: [1, 3],
    },
    retreat: {
        name: 'retreat', speed: 6, damage: 0, persist: 0, range: [0, -1],
        pa: {
            s: 'back 2',
            move: -2,
        }, eot: {
            s: 'gain 4 agility',
            agility: 4,
        },
    },
    parry: {
        name: 'parry', speed: 1, damage: 7, persist: 6, range: [1, 2],
        pa: {
            s: 'back 1',
            move: -1,
        },
    },
};

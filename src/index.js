import React from 'react';
import { createRoot } from 'react-dom/client';

import('./components/game')
    .then(module => {
        const root = createRoot(document.getElementById('root'));
        root.render(
            <React.StrictMode>
                <module.default />
            </React.StrictMode>
        );
    });

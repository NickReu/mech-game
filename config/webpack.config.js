const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const mode = process.env.JS_ENV;
const prod = mode === 'production';
module.exports = {
    entry: './src/index.js',
    mode: mode,
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: { presets: ['@babel/react'] }
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            }
        ]
    },
    devServer: {
        static: './public',
        historyApiFallback: true,
    },
    plugins: [
        new MiniCssExtractPlugin(),
        new webpack.DefinePlugin({
            'NPM_PACKAGE_VERSION': JSON.stringify(process.env.npm_package_version),
        }),
    ],
    resolve: {
        alias: {
            src: path.resolve(__dirname, '..', 'src/'),
        },
        extensions: ['*', '.js', '.jsx'],
    },
    output: {
        path: path.resolve(__dirname, '..', 'build/'),
        filename: 'bundle.js'
    }
};
